const players = [
  {
    id: 1,
    name: "Wallacce",
    score: 0,
  },
  {
    id: 2,
    name: "Per",
    score: 0,
  },
];

let diceValue = 0;
let player1Turn = true;

const messageEl = document.getElementById("message");
const player1ScoreboardEl = document.getElementById("player1Scoreboard");
const player2ScoreboardEl = document.getElementById("player2Scoreboard");
const player1DiceEl = document.getElementById("player1Dice");
const player2DiceEl = document.getElementById("player2Dice");
const rollBtn = document.getElementById("rollBtn");
const resetBtn = document.getElementById("resetBtn");

rollBtn.addEventListener("click", setPlayer);

//save data into players array
function setPlayer() {
  diceValue = rollDice();
  if (player1Turn) {
    players[0].score += diceValue;
    getPlayer();
    isWinner();
    player1Turn = !player1Turn;
  } else {
    players[1].score += diceValue;
    getPlayer();
    isWinner();
    player1Turn = !player1Turn;
  }
}

//fetch and display data
function getPlayer() {
  if (player1Turn) {
    player1DiceEl.textContent = diceValue;
    player1ScoreboardEl.textContent = players[0].score;
    nextPlayer();
  } else {
    player2DiceEl.textContent = diceValue;
    player2ScoreboardEl.textContent = players[1].score;
    nextPlayer();
  }
}


function rollDice() {
  let value = getNumber();
  return value;
}

function getNumber() {
  let randomNum = Math.floor(Math.random() * 6) + 1;
  return randomNum;
}

//alternate player's name and active class for the dice
function nextPlayer() {
  if (player1Turn) {
    message.textContent = players[1].name + "'s turn";
    player1DiceEl.classList.remove("active");
    player2DiceEl.classList.add("active");
  } else {
    message.textContent = players[0].name + "'s turn";
    player2DiceEl.classList.remove("active");
    player1DiceEl.classList.add("active");
  }
}

//display message and buttons according to condition
function isWinner() {
  if (players[0].score >= 22) {
    getWinner(0);
  } else if (players[1].score >= 22) {
    getWinner(1);
  }
}

function getWinner(index){
    messageEl.textContent = players[index].name + " wins!!!";
    resetBtn.style.display="inline";
    rollBtn.classList.add("display-none");
}

//initial state
function renderGame() {
  message.textContent = players[0].name + "'s turn";
  player1Turn = true;
  rollBtn.classList.remove("display-none");

}

resetBtn.addEventListener("click", resetGame);

function resetGame() {
    for (let player of players) {
        player.score = 0;
    }
    player1DiceEl.textContent = "-";
    player2DiceEl.textContent = "-";
    player1ScoreboardEl.textContent = 0;
    player2ScoreboardEl.textContent = 0;
    resetBtn.style.display="none";
    player2DiceEl.classList.remove("active");
    
    
    renderGame()

}
renderGame();
